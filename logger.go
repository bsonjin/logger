package logger

import (
	"fmt"
	defaultLog "log"
	"os"
	"path/filepath"
	"runtime"
	"strings"
)

var defaultLogger *defaultLog.Logger

var Log Logger

type Logger struct {
	DebugMode    bool
	LogFolderNum int
}

func init() {
	file := Log.SetOutput("default")
	defaultLog.SetOutput(file)
	defaultLogger = defaultLog.New(file, "INFO ", defaultLog.Ldate|defaultLog.Ltime)

	Log.SetDebug(true)
	Log.SetFolderNum(3)
}

// New creates a logger object
// logFile is the name of the log
func New(logFile string) Logger {
	var log Logger

	file := log.SetOutput(logFile)
	defaultLog.SetOutput(file)
	defaultLogger = defaultLog.New(file, "INFO ", defaultLog.Ldate|defaultLog.Ltime)

	log.SetDebug(true)
	log.SetFolderNum(3)

	return log
}

func (Log *Logger) SetOutput(logFile string) *os.File {
	err := os.MkdirAll(filepath.Dir(logFile), 0777)
	if err != nil {
		defaultLog.SetPrefix("[FATAL] ")
		defaultLog.Fatalln("Failed to make dir for log file", err)
	}

	file, err := os.OpenFile(logFile+".log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		defaultLog.SetPrefix("[FATAL] ")
		defaultLog.Fatalln("Failed to open log file", err)
	}
	defaultLog.SetOutput(file)
	defaultLogger = defaultLog.New(file, "INFO ", defaultLog.Ldate|defaultLog.Ltime)

	return file
}

func (Log *Logger) SetDebug(debugMode bool) {
	Log.DebugMode = debugMode
}

func (Log *Logger) SetFolderNum(folderNum int) {
	Log.LogFolderNum = folderNum
}

// for logging
func (Log *Logger) Info(err ...interface{}) {
	printDebug("[INFO]", Log.DebugMode, Log.LogFolderNum, err...)
}

func (Log Logger) Danger(err ...interface{}) {
	printDebug("[ERROR]", Log.DebugMode, Log.LogFolderNum, err...)
}

func (Log Logger) Fatal(err ...interface{}) {
	printDebug("[FATAL]", Log.DebugMode, Log.LogFolderNum, err...)
}

func (Log Logger) Warning(err ...interface{}) {
	printDebug("[WARNING]", Log.DebugMode, Log.LogFolderNum, err...)
}

// ShorterPath helps to shorten the path name by limiting n numbers of parent folders
// If path := /hello/world/myfile.txt
// ShorterPath(path, 0) means get file name only i.e. myfile.txt
// ShorterPath(path, 1) means get file name with 1 parent folder i.e. world/myfile.txt
// ShorterPath(path, 2) means get file name with 2 parent folder i.e. /hello/world/myfile.txt
func ShorterPath(path string, n int) string {
	pathArr := strings.Split(path, "/")
	pathArrLen := len(pathArr)

	if n >= 0 && pathArrLen > 1 && pathArrLen > n {
		parentFolder := pathArr[pathArrLen-n-1:] // Get n parent folder
		return strings.Join(parentFolder[:], "/")
	}
	return path
}

// printDebug will print and log the error messages
// debugMode true will print msg in console as well as log to file
// logFolderNum specfies the number of parent folder to log
func printDebug(prefix string, debugMode bool, logFolderNum int, err ...interface{}) {
	// Get the previous file that has error
	_, fn, line, _ := runtime.Caller(2)

	// Convert err msg into string
	errStr := ""
	for _, e := range err {
		errStr += fmt.Sprintf("%s ", e)
	}

	fmt.Println(fn, logFolderNum, ShorterPath(fn, logFolderNum))

	// Create custom error information
	lineInfo := fmt.Sprintf("%s:%d: %s", ShorterPath(fn, logFolderNum), line, errStr)

	// Print in console
	if debugMode == true {
		fmt.Printf("%s %s\n", prefix, lineInfo)
	}

	// Log to file
	defaultLogger.SetPrefix(prefix + " ")
	if prefix == "FATAL" {
		defaultLogger.Fatalln(lineInfo)
	} else {
		defaultLogger.Println(lineInfo)
	}
}
