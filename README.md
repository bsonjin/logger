# Logger



## Getting started
import the library
```
import logger "gitlab.com/bsonjin/logger"
```

## Default Log path
```
./default.log
```

## Log type includes Info / Warning / Danger / Fatal
```
logger.Log.Info("Hello Info")
logger.Log.Warning("Hello Info")
logger.Log.Danger("Hello Danger")
logger.Log.Fatal("Hello Fatal")
```

## Create new log object
```
newLog := logger.New("log_path/log_name")
newLog.Info("Hello New Log")
```

## Set debug mode True (DEFAULT) / False
### True:  print the log on console
### False: don't print the log on console
```
Log.SetLogDebug(false)
```

## Set log folder number
### limit the numbers of folders to show in log file information
### for details, read ShorterPath()
```
Log.SetLogFolderNum(5)
```